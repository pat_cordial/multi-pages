# GitLab Multi-Version Pages Recipe

GitLab Pages is great for publishing static websites, but only one version of the website can be published at a time. There exists an [issue that requests multi-version Pages support](https://gitlab.com/gitlab-org/gitlab/-/issues/16208), but it has been open for years with little progress.

This repository provides a recipe how to deploy multiple versions of a website using GitLab Pages.

## How it works

Each branch for which a version of the website shall be published builds the website in a CI job named `multi-pages`. As with the standard `pages` job, the files must be placed in the directory `public/` and this directory must be an artifact of the CI job. In a subsequent stage, a CI job named `pages-trigger` triggers the CI pipeline of the branch `pages`. This pipeline contains a CI job named `pages` (a standard GitLab Pages job) which collects the artifacts for all branches (except `pages`) whose latest CI pipeline had a CI job named `multi-pages` and puts their contents in the `public/` folder. Specifically, the contents of the artifact from the default branch are placed directly in `public/` and the contents of the artifacts from all other branches are placed in `public/-/<BRANCH_NAME>/`. When a branch name contains a slash (`/`), it is replaced by `+` to avoid nested directories and possible name collisions.

This recipe requires only `CI_JOB_TOKEN` for authentication.

## Configuration

There are a few (optional) settings for customization via CI variables:

- `MULTI_PAGES_JOB_NAME`: The name of the CI job that builds a version of the website. Default: `multi-pages`
- `MULTI_PAGES_PATH_PREFIX`: The URL path prefix at which the websites for non-default branches are accessible. Default: `/-`

## Contributions

Suggestions for improvement are most welcome. Feel free to create an issue via the issue tracker or to submit a MR.
